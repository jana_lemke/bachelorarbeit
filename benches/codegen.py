#!/usr/bin/env python3

from os import listdir
from os.path import isfile, basename, join

files = [
    f
    for f in listdir("dimacs")
    if isfile(join("dimacs", f)) and "dimacs" in f and not "barabasi" in f
]
heuristics = [
    "Matchbased",
    "MatchbasedHighestFirst",
    "MatchbasedLowestFirst",
    "DFSTreeBased",
]


def time():
    macros = list()
    targets = list()
    for f in files:
        for h in heuristics:
            name = f[:-7].replace("-", "_") + "_" + h
            macros.append(
                f"""bench_dimacs_file!(
            {name},
            \"dimacs/{f}\",
            {h}::new(),
            time
            );
                """
            )
            targets.append(name)
    return (macros, targets)


def nodes():
    macros = list()
    targets = list()
    for f in files:
        for h in heuristics:
            name = f[:-7].replace("-", "_") + "_" + h
            macros.append(
                f"""bench_dimacs_file!(
            {name},
            \"dimacs/{f}\",
            {h}::new(),
            nodes
            );
                """
            )
            targets.append(name)
    return (macros, targets)


if __name__ == "__main__":
    (time_macros, time_targets) = time()
    (node_macros, node_targets) = nodes()
    with open("dimacs.rs", "w") as f:
        f.write(
            """use branchbound::{
    heuristic::{DFSTreeBased, Matchbased, MatchbasedHighestFirst, MatchbasedLowestFirst},
    problem::VertexCover,
    solver::{Solver, Statistics, StatisticsWrapper},
    dimacs_meta::*,
    custom_criterion_group,
    bench_dimacs_file,
};
use criterion::{criterion_group, criterion_main, Criterion};

        mod time{
        use super::*;

"""
        )
        for m in time_macros:
            f.write(m)
        f.write(
            """
        }

        mod nodes {
        use super::*;

        """
        )
        for m in node_macros:
            f.write(m)
        f.write(
            """
        }

        criterion_group!(name = time; config = Criterion::default().sample_size(SAMPLE_SIZE); targets = """
        )
        for t in time_targets:
            f.write("time::" + t + ",")

        f.write(
            """);
        custom_criterion_group!(name = nodes; stats ;config = Criterion::default().sample_size(SAMPLE_SIZE); targets = """
        )
        for t in node_targets:
            f.write("nodes::" + t + ",")

        f.write(
            """);
        criterion_main!(time, nodes);
        """
        )
