use branchbound::{
    heuristic::{DFSTreeBased, Matchbased, MatchbasedHighestFirst, MatchbasedLowestFirst},
    problem::VertexCover,
    solver::{Solver, Statistics, StatisticsWrapper},
};
use criterion::{BatchSize, BenchmarkId, Criterion};
fn random_graph_nodes(c: &mut Criterion<StatisticsWrapper>, stats: &Statistics) {
    let mut group = c.benchmark_group("nodes/random/matchbased");
    for size in &[10, 15, 20, 25, 30] {
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            b.iter_batched(
                || VertexCover::random(*size).with_cache(),
                |instance| {
                    Solver::default()
                        .with_statistics(stats)
                        .branch_and_bound(instance, Matchbased::new())
                },
                BatchSize::SmallInput,
            );
        });
    }
    group.finish();
}

fn random_graph_nodes_lowest(c: &mut Criterion<StatisticsWrapper>, stats: &Statistics) {
    let mut group = c.benchmark_group("nodes/random/matchbased_lowest");
    for size in &[10, 15, 20, 25, 30] {
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            b.iter_batched(
                || VertexCover::random(*size).with_cache(),
                |instance| {
                    Solver::default()
                        .with_statistics(stats)
                        .branch_and_bound(instance, MatchbasedLowestFirst::new())
                },
                BatchSize::SmallInput,
            );
        });
    }
    group.finish();
}

fn random_graph_nodes_highest(c: &mut Criterion<StatisticsWrapper>, stats: &Statistics) {
    let mut group = c.benchmark_group("nodes/random/matchbased_highest");
    for size in &[10, 15, 20, 25, 30] {
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            b.iter_batched(
                || VertexCover::random(*size).with_cache(),
                |instance| {
                    Solver::default()
                        .with_statistics(stats)
                        .branch_and_bound(instance, MatchbasedHighestFirst::new())
                },
                BatchSize::SmallInput,
            );
        });
    }
    group.finish();
}

fn random_graph_nodes_dfs(c: &mut Criterion<StatisticsWrapper>, stats: &Statistics) {
    let mut group = c.benchmark_group("nodes/random/dfstree");
    for size in &[10, 15, 20, 25, 30] {
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            b.iter_batched(
                || VertexCover::random(*size).with_cache(),
                |instance| {
                    Solver::default()
                        .with_statistics(stats)
                        .branch_and_bound(instance, DFSTreeBased::new())
                },
                BatchSize::SmallInput,
            );
        });
    }
    group.finish();
}

fn main() {
    let sample_size = 20;
    let stats = Statistics::default();
    let mut criterion = Criterion::default()
        .configure_from_args()
        .with_measurement(StatisticsWrapper::new(&stats))
        .sample_size(sample_size);
    random_graph_nodes(&mut criterion, &stats);

    let mut criterion = Criterion::default()
        .configure_from_args()
        .with_measurement(StatisticsWrapper::new(&stats))
        .sample_size(sample_size);
    random_graph_nodes_lowest(&mut criterion, &stats);

    let mut criterion = Criterion::default()
        .configure_from_args()
        .with_measurement(StatisticsWrapper::new(&stats))
        .sample_size(sample_size);
    random_graph_nodes_highest(&mut criterion, &stats);

    let mut criterion = Criterion::default()
        .configure_from_args()
        .with_measurement(StatisticsWrapper::new(&stats))
        .sample_size(sample_size);
    random_graph_nodes_dfs(&mut criterion, &stats);

    Criterion::default().configure_from_args().final_summary();
}
