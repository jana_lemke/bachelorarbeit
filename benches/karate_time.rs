use branchbound::{
    heuristic::{DFSTreeBased, Matchbased, MatchbasedHighestFirst, MatchbasedLowestFirst},
    problem::VertexCover,
    solver::Solver,
};
use criterion::{criterion_group, criterion_main, Criterion};

fn karate_graph_time(c: &mut Criterion) {
    let instance = VertexCover::karate_graph().with_cache();
    c.bench_function("time/karate/matchbased/.", |b| {
        b.iter_batched(
            || instance.clone(),
            |instance| Solver::default().branch_and_bound(instance, Matchbased::new()),
            criterion::BatchSize::SmallInput,
        )
    });
}

fn karate_graph_time_lowest(c: &mut Criterion) {
    let instance = VertexCover::karate_graph().with_cache();
    c.bench_function("time/karate/matchbased_lowest/.", |b| {
        b.iter_batched(
            || instance.clone(),
            |instance| Solver::default().branch_and_bound(instance, MatchbasedLowestFirst::new()),
            criterion::BatchSize::SmallInput,
        )
    });
}

fn karate_graph_time_highest(c: &mut Criterion) {
    let instance = VertexCover::karate_graph().with_cache();
    c.bench_function("time/karate/matchbased_highest/.", |b| {
        b.iter_batched(
            || instance.clone(),
            |instance| Solver::default().branch_and_bound(instance, MatchbasedHighestFirst::new()),
            criterion::BatchSize::SmallInput,
        )
    });
}

fn karate_graph_time_dfs(c: &mut Criterion) {
    let instance = VertexCover::karate_graph().with_cache();
    c.bench_function("time/karate/dfstree/.", |b| {
        b.iter_batched(
            || instance.clone(),
            |instance| Solver::default().branch_and_bound(instance, DFSTreeBased::new()),
            criterion::BatchSize::SmallInput,
        )
    });
}

criterion_group!(name = time; config = Criterion::default().sample_size(10); targets = karate_graph_time, karate_graph_time_lowest, karate_graph_time_highest, karate_graph_time_dfs);
criterion_main!(time);
