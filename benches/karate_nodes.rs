use branchbound::{
    heuristic::{DFSTreeBased, Matchbased, MatchbasedHighestFirst, MatchbasedLowestFirst},
    problem::VertexCover,
    solver::{Solver, Statistics, StatisticsWrapper},
};
use criterion::{BatchSize, Criterion};

fn karate_graph_nodes(c: &mut Criterion<StatisticsWrapper>, stats: &Statistics) {
    let instance = VertexCover::karate_graph().with_cache();
    let mut group = c.benchmark_group("nodes/karate/matchbased");
    group.bench_function(".", |b| {
        b.iter_batched(
            || instance.clone(),
            |instance| {
                Solver::default()
                    .with_statistics(stats)
                    .branch_and_bound(instance, Matchbased::new())
            },
            BatchSize::SmallInput,
        );
    });
    group.finish();
}
fn karate_graph_nodes_lowest(c: &mut Criterion<StatisticsWrapper>, stats: &Statistics) {
    let instance = VertexCover::karate_graph().with_cache();
    let mut group = c.benchmark_group("nodes/karate/matchbased_lowest");

    group.bench_function(".", |b| {
        b.iter_batched(
            || instance.clone(),
            |instance| {
                Solver::default()
                    .with_statistics(stats)
                    .branch_and_bound(instance, MatchbasedLowestFirst::new())
            },
            BatchSize::SmallInput,
        );
    });

    group.finish();
}

fn karate_graph_nodes_highest(c: &mut Criterion<StatisticsWrapper>, stats: &Statistics) {
    let instance = VertexCover::karate_graph().with_cache();
    let mut group = c.benchmark_group("nodes/karate/matchbased_highest");

    group.bench_function(".", |b| {
        b.iter_batched(
            || instance.clone(),
            |instance| {
                Solver::default()
                    .with_statistics(stats)
                    .branch_and_bound(instance, MatchbasedHighestFirst::new())
            },
            BatchSize::SmallInput,
        );
    });

    group.finish();
}

fn karate_graph_nodes_dfs(c: &mut Criterion<StatisticsWrapper>, stats: &Statistics) {
    let instance = VertexCover::karate_graph().with_cache();
    let mut group = c.benchmark_group("nodes/karate/dfstree");

    group.bench_function(".", |b| {
        b.iter_batched(
            || instance.clone(),
            |instance| {
                Solver::default()
                    .with_statistics(stats)
                    .branch_and_bound(instance, DFSTreeBased::new())
            },
            BatchSize::SmallInput,
        );
    });

    group.finish();
}

// static statistics: Statistics = Statistics::default();

fn main() {
    let sample_size = 10;
    let stats = Statistics::default();
    let mut criterion = Criterion::default()
        .with_measurement(StatisticsWrapper::new(&stats))
        .configure_from_args()
        .sample_size(sample_size)
        .without_plots();
    karate_graph_nodes(&mut criterion, &stats);

    let mut criterion = Criterion::default()
        .with_measurement(StatisticsWrapper::new(&stats))
        .configure_from_args()
        .sample_size(sample_size)
        .without_plots();
    karate_graph_nodes_lowest(&mut criterion, &stats);

    let mut criterion = Criterion::default()
        .with_measurement(StatisticsWrapper::new(&stats))
        .configure_from_args()
        .sample_size(sample_size)
        .without_plots();
    karate_graph_nodes_highest(&mut criterion, &stats);

    let mut criterion = Criterion::default()
        .with_measurement(StatisticsWrapper::new(&stats))
        .configure_from_args()
        .sample_size(sample_size)
        .without_plots();
    karate_graph_nodes_dfs(&mut criterion, &stats);

    Criterion::default().configure_from_args().final_summary();
}
