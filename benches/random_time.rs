use branchbound::{
    heuristic::{DFSTreeBased, Matchbased, MatchbasedHighestFirst, MatchbasedLowestFirst},
    problem::VertexCover,
    solver::Solver,
};
use criterion::{criterion_group, criterion_main, BatchSize, BenchmarkId, Criterion};

fn random_graph_time(c: &mut Criterion) {
    let mut group = c.benchmark_group("time/random/matchbased");
    for size in &[10, 15, 20, 25, 30] {
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            b.iter_batched(
                || VertexCover::random(*size).with_cache(),
                |instance| Solver::default().branch_and_bound(instance, Matchbased::new()),
                BatchSize::SmallInput,
            );
        });
    }
    group.finish();
}

fn random_graph_time_lowest(c: &mut Criterion) {
    let mut group = c.benchmark_group("time/random/matchbased_lowest");
    for size in &[10, 15, 20, 25, 30] {
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            b.iter_batched(
                || VertexCover::random(*size).with_cache(),
                |instance| {
                    Solver::default().branch_and_bound(instance, MatchbasedLowestFirst::new())
                },
                BatchSize::SmallInput,
            );
        });
    }
    group.finish();
}

fn random_graph_time_highest(c: &mut Criterion) {
    let mut group = c.benchmark_group("time/random/matchbased_highest");
    for size in &[10, 15, 20, 25, 30] {
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            b.iter_batched(
                || VertexCover::random(*size).with_cache(),
                |instance| {
                    Solver::default().branch_and_bound(instance, MatchbasedHighestFirst::new())
                },
                BatchSize::SmallInput,
            );
        });
    }
    group.finish();
}

fn random_graph_time_dfs(c: &mut Criterion) {
    let mut group = c.benchmark_group("time/random/dfstree");
    for size in &[10, 15, 20, 25, 30] {
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &size| {
            b.iter_batched(
                || VertexCover::random(*size).with_cache(),
                |instance| Solver::default().branch_and_bound(instance, DFSTreeBased::new()),
                BatchSize::SmallInput,
            );
        });
    }
    group.finish();
}

criterion_group!(name = time; config = Criterion::default().sample_size(50); targets = random_graph_time, random_graph_time_lowest, random_graph_time_highest, random_graph_time_dfs);
criterion_main!(time);
