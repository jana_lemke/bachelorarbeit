use branchbound::problem::VertexCover;
use clap::{App, Arg};
use std::{
    error::Error,
    fs::read_to_string,
    io::{stdin, Read},
};

fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("Vertex Cover Solver")
        .arg(
            Arg::with_name("format")
                .takes_value(true)
                .default_value("dimacs")
                .possible_value("dimacs")
                .possible_value("stanford")
                .possible_value("mm")
                .long("format")
                .short("f"),
        )
        .arg(
            Arg::with_name("input")
                .takes_value(true)
                .default_value("stdin")
                .long("input")
                .short("i"),
        )
        .get_matches();

    let input = matches.value_of("input").unwrap();
    println!(
        "{}",
        {
            let mut instance_string = String::new();
            match input {
                "-" | "stdin" => {
                    stdin().read_to_string(&mut instance_string)?;
                }
                path => instance_string = read_to_string(path)?,
            }
            match matches.value_of("format").unwrap() {
                "dimacs" => VertexCover::from_dimacs(&instance_string),
                "stanford" => VertexCover::from_stanford(&instance_string),
                "mm" => VertexCover::from_matrix_market(&instance_string),
                _ => panic!("aargh"),
            }
        }
        .to_gr()
    );
    Ok(())
}
