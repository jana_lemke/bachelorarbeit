use crate::{
    heuristic::Heuristic,
    problem::Solution,
    problem::{Problem, VertexCover},
};
use num_bigint::BigUint;
use std::fmt::Debug;
use std::{
    cell::RefCell,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
};

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Statistics {
    visited_nodes: RefCell<BigUint>,
}

impl std::fmt::Display for Statistics {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        let mut total: f64 = 0.0;
        for val in self.visited_nodes.borrow().to_u64_digits() {
            total *= 2.0f64.powi(64);
            total += val as f64
        }
        write!(f, "{}", total)
    }
}

impl Statistics {
    fn increment(&self) {
        *self.visited_nodes.borrow_mut() += 1usize;
    }
}

#[cfg(feature = "cut-nodes-bench")]
#[derive(Debug, Clone)]
pub struct StatisticsWrapper<'a>(&'a Statistics);

impl<'a> StatisticsWrapper<'a> {
    pub fn new(inner: &'a Statistics) -> Self {
        Self(inner)
    }
}

impl std::fmt::Display for StatisticsWrapper<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        write!(f, "{}", self.0)
    }
}
#[cfg(feature = "cut-nodes-bench")]
impl<'a> criterion::measurement::Measurement for StatisticsWrapper<'a> {
    type Intermediate = BigUint;
    type Value = BigUint;

    fn start(&self) -> Self::Intermediate {
        self.0.visited_nodes.borrow().clone()
    }
    fn end(&self, i: Self::Intermediate) -> Self::Value {
        self.0.visited_nodes.borrow().clone() - i
    }
    fn add(&self, v1: &Self::Value, v2: &Self::Value) -> Self::Value {
        v1 + v2
    }
    fn zero(&self) -> Self::Value {
        BigUint::from(0u8)
    }

    fn to_f64(&self, value: &Self::Value) -> f64 {
        let mut total: f64 = 0.0;
        for val in value.to_u64_digits() {
            total *= 2.0f64.powi(64);
            total += val as f64
        }
        total
    }

    fn formatter(&self) -> &dyn criterion::measurement::ValueFormatter {
        &StatisticsFormatter
    }
}
#[cfg(feature = "cut-nodes-bench")]
#[derive(Default)]
struct StatisticsFormatter;

#[cfg(feature = "cut-nodes-bench")]
impl criterion::measurement::ValueFormatter for StatisticsFormatter {
    fn format_value(&self, value: f64) -> String {
        format!("{:3.3e}", value)
    }

    fn format_throughput(&self, _throughput: &criterion::Throughput, _value: f64) -> String {
        unimplemented!()
    }

    fn scale_values(&self, _typical_value: f64, _values: &mut [f64]) -> &'static str {
        "nodes"
    }

    fn scale_throughputs(
        &self,
        _typical_value: f64,
        _throughput: &criterion::Throughput,
        _values: &mut [f64],
    ) -> &'static str {
        unimplemented!()
    }

    fn scale_for_machines(&self, _values: &mut [f64]) -> &'static str {
        "nodes"
    }
}

#[derive(Debug, Copy, Clone)]
enum BranchingType {
    OnEdges,
    OnNodes,
}

impl Default for BranchingType {
    fn default() -> Self {
        BranchingType::OnNodes
    }
}

#[derive(Debug, Default)]
pub struct Solver<'a> {
    stats: Option<&'a Statistics>,
    running: Option<Arc<AtomicBool>>,
    debug: bool,
    branching: BranchingType,
}

impl<'a> Solver<'a> {
    pub fn with_statistics(mut self, stats: &'a Statistics) -> Self {
        self.stats = Some(stats);
        self
    }

    pub fn with_running(mut self, running: Arc<AtomicBool>) -> Self {
        self.running = Some(running);
        self
    }

    pub fn should_run(&self) -> bool {
        self.running
            .as_ref()
            .map(|r| r.load(Ordering::SeqCst))
            .unwrap_or(true)
    }

    pub fn with_branching_on_edges(mut self) -> Self {
        self.branching = BranchingType::OnEdges;
        self
    }

    pub fn with_branching_on_nodes(mut self) -> Self {
        self.branching = BranchingType::OnNodes;
        self
    }

    pub fn with_debug(mut self, debug: bool) -> Self {
        self.debug = debug;
        self
    }
}

impl Solver<'_> {
    pub fn branch_and_bound<H>(
        &mut self,
        instance: <H as Heuristic>::Problem,
        heuristic: H,
    ) -> <<H as Heuristic>::Problem as Problem>::Solution
    where
        H: Heuristic<Problem = VertexCover> + Clone + Debug,
        <<H as Heuristic>::Problem as Problem>::Solution: Solution,
        <<H as Heuristic>::Problem as Problem>::Intermediate: Solution + Default,
        <<H as Heuristic>::Problem as Problem>::Choice: Copy + Debug,
    {
        let start = std::time::Instant::now();
        let size = instance.size();
        let mut best_sol: <<H as Heuristic>::Problem as Problem>::Intermediate = Default::default();
        let mut best_sol_size = size;
        let mut queue: Vec<H> = vec![heuristic];
        while !queue.is_empty() && self.should_run() {
            self.stats.as_mut().map(|h| {
                h.increment();
                h
            });
            let mut heuristic = queue.pop().unwrap();
            while self.should_run()
                && (heuristic.reduce_degree1(&instance) || heuristic.reduce_dominance(&instance))
            {
            }
            let result = heuristic.clone().apply(&instance);
            let result_size = result.size();
            let unassigned = heuristic.unassigned(&instance);
            if result_size >= 2 * best_sol_size {
                // we can cut the branch
                continue;
            } else if !unassigned.is_empty() {
                // we are not in a leaf node, we want to branch
                match self.branching {
                    BranchingType::OnEdges => {
                        let (left, right) = *unassigned.first().unwrap();
                        let mut h = heuristic.clone();
                        h.choose(left);
                        queue.push(h);
                        let mut h = heuristic.clone();
                        h.choose(right);
                        queue.push(h);
                    }
                    BranchingType::OnNodes => {
                        let (left, _) = *unassigned.first().unwrap();
                        let mut h = heuristic.clone();
                        h.choose(left);
                        queue.push(h);
                        let mut h = heuristic.clone();
                        for r in instance.get_neighbours(left) {
                            h.choose(r);
                        }
                        queue.push(h)
                    }
                }
            }
            if result_size < best_sol_size {
                best_sol = result;
                best_sol_size = result_size;
                if self.debug {
                    eprintln!(
                        "Found solution of size {} after {:?}",
                        best_sol_size,
                        std::time::Instant::now() - start
                    )
                }
            }
        }
        // if self.debug {
        //     assert!(Self::check_if_vertex_cover::<H>(&instance, &best_sol));
        // }
        instance.translate_solution(best_sol)
    }

    pub fn check_if_vertex_cover<H: Heuristic>(
        instance: &VertexCover,
        solution: &<VertexCover as Problem>::Intermediate,
    ) -> bool {
        instance
            .edges()
            .iter()
            .all(|(left, right)| solution.contains(left) || solution.contains(&right))
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::heuristic::Matchbased;
    use crate::problem::Solution;
    use crate::problem::VertexCover;

    #[test]
    fn solve_wikipedia() {
        let v = VertexCover::from_edges(&[(0, 1), (0, 2), (1, 2), (1, 3), (1, 4), (1, 5)]);
        let h = Matchbased::new();
        let mut stats = Statistics::default();
        let result = Solver::default()
            .with_statistics(&mut stats)
            .branch_and_bound(v, h);
        // assert_eq!(
        //     stats,
        //     Statistics {
        //         visited_nodes: RefCell::new(BigUint::from(7u32))
        //     }
        // );
        assert_eq!(result.size(), 2);
        assert_eq!(result, vec![1, 2].into_iter().collect());
    }

    #[test]
    fn solve_wikipedia_nodes() {
        let v = VertexCover::from_edges(&[(0, 1), (0, 2), (1, 2), (1, 3), (1, 4), (1, 5)]);
        let h = Matchbased::new();
        let mut stats = Statistics::default();
        let result = Solver::default()
            .with_statistics(&mut stats)
            .with_branching_on_nodes()
            .branch_and_bound(v, h);
        assert_eq!(result.size(), 2);
        assert_eq!(result, vec![1, 2].into_iter().collect());
    }

    #[test]
    fn visited_nodes() {
        let v = VertexCover::from_edges(&[(0, 1), (1, 2), (1, 3), (1, 4), (1, 5)]);
        let h = Matchbased::new();
        let mut stats = Statistics::default();
        let result = Solver::default()
            .with_statistics(&mut stats)
            .branch_and_bound(v, h);
        // assert_eq!(
        //     stats,
        //     Statistics {
        //         visited_nodes: RefCell::new(BigUint::from(3u32))
        //     }
        // );
        assert_eq!(result.size(), 1);
        assert_eq!(result, vec![1].into_iter().collect());
    }

    #[test]
    fn solve_paper() {
        let v = VertexCover::from_edges(&[
            (0, 1),
            (1, 2),
            (2, 3),
            (3, 4),
            (4, 5),
            (5, 6),
            (6, 0),
            (0, 7),
            (1, 7),
            (4, 7),
            (2, 8),
            (5, 8),
            (6, 8),
        ]);
        let h = Matchbased::new();
        let mut stats = Statistics::default();
        let result = Solver::default()
            .with_statistics(&mut stats)
            .branch_and_bound(v, h);
        // assert_eq!(
        //     stats,
        //     Statistics {
        //         visited_nodes: RefCell::new(BigUint::from(187u32))
        //     }
        // );
        assert_eq!(result.size(), 6);
        //assert_eq!(result, vec![1, 3, 5, 6, 7, 8].into_iter().collect());
    }
}
