use branchbound::problem::VertexCover;
use clap::{App, Arg};

fn main() {
    let matches = App::new("Vertex Cover Generator")
        .arg(Arg::with_name("model").required(true).possible_values(&[
            "barabasi-albert",
            "ba",
            "random",
            "ra",
        ]))
        .arg(Arg::with_name("size").required(true))
        .arg(Arg::with_name("density").required(true))
        .get_matches();
    let size: usize = matches
        .value_of("size")
        .unwrap()
        .parse()
        .expect("Could not parse size");
    let density: usize = matches
        .value_of("density")
        .unwrap()
        .parse()
        .expect("Could not parse density");
    let model = matches.value_of("model").unwrap();
    let instance = match model {
        "barabasi-albert" | "ba" => VertexCover::barabasi_albert(size, density),
        "random" | "ra" => VertexCover::random(size, density),
        _ => panic!(),
    };
    println!("{}", instance.to_dimacs())
}
