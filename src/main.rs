use branchbound::{
    heuristic::{
        DFSTreeBased, Heuristic, Matchbased, MatchbasedHighestFirst, MatchbasedLowestFirst,
    },
    problem::VertexCover,
    solver::*,
};
use clap::{App, Arg};
use std::{
    error::Error,
    fs::read_to_string,
    io::{stdin, Read},
    time::Instant,
};
use std::{
    fmt::Debug,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
};

fn solve<H: Heuristic + Clone>(
    instance: VertexCover,
    heuristic: H,
    running: Arc<AtomicBool>,
    debug: bool,
    on_nodes: bool,
) where
    H: Heuristic<Problem = VertexCover> + Clone + Debug,
{
    eprintln!("Starting to solve");
    let stats = Statistics::default();
    let start_time = Instant::now();
    let mut solver = Solver::default()
        .with_statistics(&stats)
        .with_running(running)
        .with_debug(debug);
    if on_nodes {
        solver = solver.with_branching_on_nodes();
    }
    let solution = solver.branch_and_bound(instance.clone(), heuristic);
    let finish_time = Instant::now();
    let mut sorted = solution.into_iter().collect::<Vec<_>>();
    sorted.sort_unstable();
    let taken = finish_time - start_time;
    println!("solution: {:?}", sorted);
    println!("running time (s): {:.3?}", taken.as_secs_f64());
    println!("running time (ns): {:?}", taken.as_nanos());
    println!("checked nodes: {}", stats);
    println!(
        "size of instance: {} {}",
        &instance.nodes().count(),
        &instance.edges().len()
    );
    println!("solution length: {}", sorted.len());
}

fn main() -> Result<(), Box<dyn Error>> {
    let running = Arc::new(AtomicBool::new(true));
    let r = running.clone();
    let matches = App::new("Vertex Cover Solver")
        .arg(
            Arg::with_name("format")
                .takes_value(true)
                .default_value("dimacs")
                .possible_value("dimacs")
                .possible_value("stanford")
                .possible_value("mm")
                .possible_value("gr")
                .long("format")
                .short("f"),
        )
        .arg(
            Arg::with_name("input")
                .takes_value(true)
                .default_value("stdin")
                .long("input")
                .short("i"),
        )
        .arg(
            Arg::with_name("edge-branching")
                .long("edge-branching")
                .short("E"),
        )
        .arg(
            Arg::with_name("node-branching")
                .long("node-branching")
                .short("N"),
        )
        .arg(
            Arg::with_name("heuristic")
                .takes_value(true)
                .required(true)
                .possible_value("Matchbased")
                .possible_value("MatchbasedHighest")
                .possible_value("MatchbasedLowest")
                .possible_value("DFSTree")
                .possible_value("all")
                .long("heuristic")
                .short("h"),
        )
        .arg(Arg::with_name("debug").long("debug").short("d"))
        .get_matches();

    let debug = matches.is_present("debug");
    let on_nodes = matches.is_present("node-branching") && !matches.is_present("edge-branching");
    let input = matches.value_of("input").unwrap();
    let instance = {
        let mut instance_string = String::new();
        match input {
            "-" | "stdin" => {
                stdin().read_to_string(&mut instance_string)?;
            }
            path => instance_string = read_to_string(path)?,
        }
        match matches.value_of("format").unwrap() {
            "dimacs" => VertexCover::from_dimacs(&instance_string),
            "stanford" => VertexCover::from_stanford(&instance_string),
            "mm" => VertexCover::from_matrix_market(&instance_string),
            "gr" => VertexCover::from_gr(&instance_string),
            _ => panic!("aargh"),
        }
    }
    .with_cache();

    eprintln!("Succesfully read {}", input);

    let heuristic_name = matches.value_of("heuristic").unwrap();
    ctrlc::set_handler(move || {
        r.store(false, Ordering::SeqCst);
        eprintln!("Caught Ctrl-C")
    })
    .expect("Failed to set Ctrl-C handler");
    match heuristic_name {
        "Matchbased" => solve(instance, Matchbased::new(), running, debug, on_nodes),
        "MatchbasedLowest" => solve(
            instance,
            MatchbasedLowestFirst::new(),
            running,
            debug,
            on_nodes,
        ),
        "MatchbasedHighest" => solve(
            instance,
            MatchbasedHighestFirst::new(),
            running,
            debug,
            on_nodes,
        ),
        "DFSTree" => solve(instance, DFSTreeBased::new(), running, debug, on_nodes),
        "all" => {
            println!("[Matchbased]");
            solve(
                instance.clone(),
                Matchbased::new(),
                running.clone(),
                debug,
                on_nodes,
            );
            println!("[MatchbasedHighest]");
            solve(
                instance.clone(),
                MatchbasedLowestFirst::new(),
                running.clone(),
                debug,
                on_nodes,
            );
            println!("[MatchbasedLowest]");
            solve(
                instance.clone(),
                MatchbasedHighestFirst::new(),
                running.clone(),
                debug,
                on_nodes,
            );
            println!("[DFSTree]");
            solve(instance, DFSTreeBased::new(), running, debug, on_nodes);
        }
        _ => panic!("aargh"),
    };
    Ok(())
}
