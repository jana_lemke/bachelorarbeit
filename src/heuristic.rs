use crate::problem;
use petgraph::stable_graph::NodeIndex;
use std::collections::HashSet;

/// Something that can be used as a heuristic in solving `Problem`.
///
/// Currently it is assumed that the heuristic provides a 2-approximation.
/// A lot of the details of this trait probably only make sense in the context of Minimum Vertex Cover.
pub trait Heuristic {
    /// The problem this heuristic works with.
    type Problem: problem::Problem;

    /// Mark a selection by the branch-and-bound-algorithm.
    fn choose(&mut self, choice: <Self::Problem as problem::Problem>::Choice);

    /// Return the amount of choices marked by the branch-and-bound-algorithm.
    fn size(&self) -> usize;

    /// Apply the degree-1 reduction from https://arxiv.org/pdf/1411.2680.pdf
    fn reduce_degree1(&mut self, problem: &Self::Problem) -> bool;

    /// Apply the dominance reduction from https://arxiv.org/pdf/1411.2680.pdf
    fn reduce_dominance(&mut self, problem: &Self::Problem) -> bool;

    /// Use the heuristic to get an approximate solution to the problem.
    fn apply(self, problem: &Self::Problem) -> <Self::Problem as problem::Problem>::Intermediate;

    /// Return edges where one endpoint still needs to be assigned.
    fn unassigned(
        &self,
        problem: &Self::Problem,
    ) -> Vec<(
        <Self::Problem as problem::Problem>::Choice,
        <Self::Problem as problem::Problem>::Choice,
    )>;
}

/// Implements shared behaviour between the other heuristics.
/// This is mostly to make sure that behaviour unrelated to the intended difference is consistent.
/// This should probably be instead solved by having a variable type saved in Base (those can be zerosized and essentially be a marker) and delegating behaviour from Base to that variable type instead of the other way around.
/// Yet here we are.
#[derive(Debug, Clone, Default)]
struct Base {
    chosen: HashSet<NodeIndex>,
}

impl Heuristic for Base {
    type Problem = problem::VertexCover;
    fn choose(&mut self, choice: <Self::Problem as problem::Problem>::Choice) {
        self.chosen.insert(choice);
    }

    fn size(&self) -> usize {
        self.chosen.len()
    }

    fn reduce_degree1(&mut self, problem: &Self::Problem) -> bool {
        let mut updated = false;
        let mut to_update: Vec<_> = problem.nodes().filter(|v| !self.is_chosen(v)).collect();
        while let Some(node) = to_update.pop() {
            if !self.is_chosen(&node) {
                let mut neighbours = self.get_unchosen_neighbours(problem, node);
                // the node we are looking at has at least one neighbour
                if let Some(nb) = neighbours.next() {
                    // and it has exactly on neighbour
                    if neighbours.next().is_none() {
                        drop(neighbours);
                        // so that neighbour needs to be in a vertex cover
                        self.choose(nb);
                        updated = true;
                        // and we need to check it's neighbours again
                        for u in self.get_unchosen_neighbours(problem, nb) {
                            to_update.push(u);
                        }
                    }
                }
            }
        }
        updated
    }

    /// Has no significant performance difference between a queue and just going over all vertices multiple times (in multiple calls) so the former is used
    fn reduce_dominance(&mut self, problem: &Self::Problem) -> bool {
        let mut updated = false;
        let mut to_update: Vec<_> = problem.nodes().filter(|v| !self.is_chosen(v)).collect();
        // find a node we need to update
        while let Some(v) = to_update.pop() {
            if !self.is_chosen(&v) {
                let nv: Vec<_> = self.get_unchosen_neighbours(problem, v).collect();
                for u in self.get_unchosen_neighbours(problem, v).collect::<Vec<_>>() {
                    // check if v is dominated by u ie all vertices in N[v] are in N[u] i.e. either are u or are connected to u
                    if nv.iter().all(|&nu| nu == u || problem.contains_edge(u, nu)) {
                        self.choose(u);
                        updated = true;
                        for w in self.get_unchosen_neighbours(problem, u) {
                            to_update.push(w);
                        }
                        break;
                    }
                }
            }
        }
        updated
    }

    fn apply(
        mut self,
        problem: &Self::Problem,
    ) -> <Self::Problem as problem::Problem>::Intermediate {
        for (left, right) in problem.edges() {
            if !(self.chosen.contains(&left) || self.chosen.contains(&right)) {
                self.chosen.insert(left);
                self.chosen.insert(right);
            }
        }
        self.chosen
    }

    fn unassigned(
        &self,
        problem: &Self::Problem,
    ) -> Vec<(
        <Self::Problem as problem::Problem>::Choice,
        <Self::Problem as problem::Problem>::Choice,
    )> {
        let mut unassigned = Vec::new();
        for (left, right) in problem.edges() {
            if !(self.chosen.contains(&left) || self.chosen.contains(&right)) {
                unassigned.push((left, right))
            }
        }
        unassigned
    }
}

impl Base {
    fn is_chosen(&self, node: &NodeIndex) -> bool {
        self.chosen.contains(node)
    }

    fn _is_covered(&self, node: NodeIndex, problem: &<Self as Heuristic>::Problem) -> bool {
        self.chosen.contains(&node) || problem.get_neighbours(node).all(|n| self.is_chosen(&n))
    }

    /// We do not modify the graph and instead figure out "on the fly" which vertices are still relevant
    /// This is a helper that gives us all neighbours that are not yet decided to be in the vertex cover and therefore still relevant.
    fn get_unchosen_neighbours<'a>(
        &'a self,
        problem: &'a <Self as Heuristic>::Problem,
        v: NodeIndex,
    ) -> impl Iterator<Item = NodeIndex> + 'a {
        problem
            .get_neighbours(v)
            .filter(move |&u| !self.is_chosen(&u))
    }
}

/// Heuristic for Vertex Cover based on Matchings.
/// Repeatedly picks a not yet covered edge and adds both of it's endpoints to the solution.
#[derive(Debug, Clone, Default)]
pub struct Matchbased {
    inner: Base,
}

impl Matchbased {
    pub fn new() -> Self {
        Matchbased {
            inner: Base::default(),
        }
    }
}

impl std::fmt::Display for Matchbased {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{:?}", self.inner.chosen)
    }
}

impl Heuristic for Matchbased {
    type Problem = problem::VertexCover;

    fn choose(&mut self, choice: <Self::Problem as problem::Problem>::Choice) {
        self.inner.choose(choice)
    }

    fn size(&self) -> usize {
        self.inner.size()
    }

    fn reduce_degree1(&mut self, problem: &Self::Problem) -> bool {
        self.inner.reduce_degree1(problem)
    }

    fn reduce_dominance(&mut self, problem: &Self::Problem) -> bool {
        self.inner.reduce_dominance(problem)
    }

    fn apply(
        mut self,
        problem: &Self::Problem,
    ) -> <Self::Problem as problem::Problem>::Intermediate {
        for (left, right) in problem.edges() {
            if !(self.inner.chosen.contains(&left) || self.inner.chosen.contains(&right)) {
                self.inner.chosen.insert(left);
                self.inner.chosen.insert(right);
            }
        }
        self.inner.chosen
    }

    fn unassigned(
        &self,
        problem: &Self::Problem,
    ) -> Vec<(
        <Self::Problem as problem::Problem>::Choice,
        <Self::Problem as problem::Problem>::Choice,
    )> {
        self.inner.unassigned(&problem)
    }
}

/// Heuristic for Vertex Cover based on Matchings.
/// Repeatedly picks the not yet covered edge with the highest sum of degrees of it's endpoints and adds both of it's endpoints to the solution.
#[derive(Debug, Clone, Default)]
pub struct MatchbasedHighestFirst {
    inner: Base,
}

impl Heuristic for MatchbasedHighestFirst {
    type Problem = problem::VertexCover;

    fn choose(&mut self, choice: <Self::Problem as problem::Problem>::Choice) {
        self.inner.choose(choice)
    }

    fn size(&self) -> usize {
        self.inner.size()
    }

    fn reduce_degree1(&mut self, problem: &Self::Problem) -> bool {
        self.inner.reduce_degree1(problem)
    }

    fn reduce_dominance(&mut self, problem: &Self::Problem) -> bool {
        self.inner.reduce_dominance(problem)
    }

    fn apply(
        mut self,
        problem: &Self::Problem,
    ) -> <Self::Problem as problem::Problem>::Intermediate {
        for &(left, right) in problem.sorted_edges().iter().rev() {
            if !(self.inner.chosen.contains(&left) || self.inner.chosen.contains(&right)) {
                self.inner.chosen.insert(left);
                self.inner.chosen.insert(right);
            }
        }
        self.inner.chosen
    }

    fn unassigned(
        &self,
        problem: &Self::Problem,
    ) -> Vec<(
        <Self::Problem as problem::Problem>::Choice,
        <Self::Problem as problem::Problem>::Choice,
    )> {
        self.inner.unassigned(&problem)
    }
}

impl MatchbasedHighestFirst {
    pub fn new() -> Self {
        MatchbasedHighestFirst {
            inner: Base::default(),
        }
    }
}

/// Heuristic for Vertex Cover based on Matchings.
/// Repeatedly picks the not yet covered edge with the lowest sum of degrees of it's endpoints and adds both of it's endpoints to the solution.
#[derive(Debug, Clone, Default)]
pub struct MatchbasedLowestFirst {
    inner: Base,
}

impl Heuristic for MatchbasedLowestFirst {
    type Problem = problem::VertexCover;

    fn choose(&mut self, choice: <Self::Problem as problem::Problem>::Choice) {
        self.inner.choose(choice)
    }

    fn size(&self) -> usize {
        self.inner.size()
    }

    fn reduce_degree1(&mut self, problem: &Self::Problem) -> bool {
        self.inner.reduce_degree1(problem)
    }

    fn reduce_dominance(&mut self, problem: &Self::Problem) -> bool {
        self.inner.reduce_dominance(problem)
    }

    fn apply(
        mut self,
        problem: &Self::Problem,
    ) -> <Self::Problem as problem::Problem>::Intermediate {
        for (left, right) in problem.sorted_edges() {
            if !(self.inner.chosen.contains(&left) || self.inner.chosen.contains(&right)) {
                self.inner.chosen.insert(left);
                self.inner.chosen.insert(right);
            }
        }
        self.inner.chosen
    }

    fn unassigned(
        &self,
        problem: &Self::Problem,
    ) -> Vec<(
        <Self::Problem as problem::Problem>::Choice,
        <Self::Problem as problem::Problem>::Choice,
    )> {
        self.inner.unassigned(&problem)
    }
}

impl MatchbasedLowestFirst {
    pub fn new() -> Self {
        MatchbasedLowestFirst {
            inner: Base::default(),
        }
    }
}

/// Heuristic for Vertex Cover based on  Depth-First-Search-Trees.
/// It calculates a DFS-Tree and adds all inner, non-leaf-nodes to the solution
#[derive(Debug, Clone, Default)]
pub struct DFSTreeBased {
    inner: Base,
}

impl Heuristic for DFSTreeBased {
    type Problem = problem::VertexCover;

    fn choose(&mut self, choice: <Self::Problem as problem::Problem>::Choice) {
        self.inner.choose(choice)
    }

    fn size(&self) -> usize {
        self.inner.size()
    }

    fn reduce_degree1(&mut self, problem: &Self::Problem) -> bool {
        self.inner.reduce_degree1(problem)
    }

    fn reduce_dominance(&mut self, problem: &Self::Problem) -> bool {
        self.inner.reduce_dominance(problem)
    }

    fn apply(
        mut self,
        problem: &Self::Problem,
    ) -> <Self::Problem as problem::Problem>::Intermediate {
        for node in problem.nodes() {
            if !self.inner.chosen.contains(&node)
                && problem
                    .get_neighbours(node)
                    .any(|x| !self.inner.chosen.contains(&x))
            {
                self.inner.chosen.insert(node);
            }
        }
        self.inner.chosen
    }

    fn unassigned(
        &self,
        problem: &Self::Problem,
    ) -> Vec<(
        <Self::Problem as problem::Problem>::Choice,
        <Self::Problem as problem::Problem>::Choice,
    )> {
        self.inner.unassigned(&problem)
    }
}

impl DFSTreeBased {
    pub fn new() -> Self {
        Self {
            inner: Base::default(),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::problem::Problem;
    use crate::problem::Solution;
    use crate::problem::VertexCover;

    #[test]
    fn apply_wikipedia() {
        let v = VertexCover::from_edges(&[(0, 1), (0, 2), (1, 2), (1, 3), (1, 4), (1, 5)]);
        let h = Matchbased::new();
        let result = h.apply(&v);
        assert_eq!(result.size(), 2);
        assert_eq!(
            v.translate_solution(result),
            vec![0, 1].into_iter().collect()
        );
    }

    #[test]
    fn apply_paper() {
        let v = VertexCover::from_edges(&[
            (0, 1),
            (1, 2),
            (2, 3),
            (3, 4),
            (4, 5),
            (5, 6),
            (6, 0),
            (0, 7),
            (1, 7),
            (4, 7),
            (2, 8),
            (5, 8),
            (6, 8),
        ]);
        let h = Matchbased::new();
        let result = h.apply(&v);
        assert_eq!(result.size(), 8);
        assert_eq!(
            v.translate_solution(result),
            vec![0, 1, 2, 3, 4, 5, 6, 8].into_iter().collect()
        );
    }

    #[test]
    fn apply_wikipedia_lowest() {
        let v = VertexCover::from_edges(&[(0, 1), (0, 2), (1, 2), (1, 3), (1, 4), (1, 5)]);
        let h = MatchbasedLowestFirst::new();
        let result = h.apply(&v);
        assert_eq!(result.size(), 4);
        assert_eq!(
            v.translate_solution(result),
            vec![0, 1, 2, 3].into_iter().collect()
        );
    }

    #[test]
    fn apply_wikipedia_highest() {
        let v = VertexCover::from_edges(&[(0, 1), (0, 2), (1, 2), (1, 3), (1, 4), (1, 5)]);
        let h = MatchbasedHighestFirst::new();
        let result = h.apply(&v);
        assert_eq!(result.size(), 2);
        assert_eq!(
            v.translate_solution(result),
            vec![1, 2].into_iter().collect()
        );
    }
    #[test]
    fn apply_wikipedia_dfs() {
        let v = VertexCover::from_edges(&[(0, 1), (0, 2), (1, 2), (1, 3), (1, 4), (1, 5)]);
        let h = DFSTreeBased::new();
        let result = h.apply(&v);
        assert_eq!(result.size(), 2);
        assert_eq!(
            v.translate_solution(result),
            vec![0, 1].into_iter().collect()
        );
    }

    #[test]
    fn apply_paper_dfs() {
        let v = VertexCover::from_edges(&[
            (0, 1),
            (1, 2),
            (2, 3),
            (3, 4),
            (4, 5),
            (5, 6),
            (6, 0),
            (0, 7),
            (1, 7),
            (4, 7),
            (2, 8),
            (5, 8),
            (6, 8),
        ]);
        let h = DFSTreeBased::new();
        let result = h.apply(&v);
        assert_eq!(result.size(), 7);
        assert_eq!(
            v.translate_solution(result),
            vec![0, 1, 2, 3, 4, 5, 6].into_iter().collect()
        );
    }

    #[test]
    fn remove_degree1_vertices_5() {
        let mut v = VertexCover::from_size(5);
        v.add_edge(0, 1);
        v.add_edge(0, 2);
        v.add_edge(1, 2);
        v.add_edge(1, 3);
        v.add_edge(2, 4);
        let mut h = Base::default();
        assert!(h.reduce_degree1(&v));
        assert_eq!(
            h.chosen,
            vec![1, 2].iter().map(|i| NodeIndex::new(*i)).collect()
        );
        assert!(!h.reduce_degree1(&v));
    }

    #[test]
    fn remove_degree1_vertices_11() {
        let mut v = VertexCover::from_size(11);
        v.add_edge(0, 1);
        v.add_edge(0, 2);
        v.add_edge(1, 2);
        v.add_edge(2, 4);
        v.add_edge(2, 5);
        v.add_edge(4, 5);
        v.add_edge(1, 3);
        v.add_edge(4, 6);
        v.add_edge(6, 9);
        v.add_edge(4, 7);
        v.add_edge(7, 10);
        v.add_edge(5, 8);
        let mut h = Base::default();
        assert!(h.reduce_degree1(&v));
        assert_eq!(
            h.chosen,
            vec![1, 2, 5, 6, 7,]
                .iter()
                .map(|i| NodeIndex::new(*i))
                .collect()
        );
        assert!(!h.reduce_degree1(&v));
    }

    #[test]
    fn remove_dominant_vertices() {
        let mut v = VertexCover::from_size(9);
        v.add_edge(0, 1);
        v.add_edge(0, 2);
        v.add_edge(0, 3);
        v.add_edge(0, 4);
        v.add_edge(0, 8);
        v.add_edge(1, 2);
        v.add_edge(2, 3);
        v.add_edge(3, 4);
        v.add_edge(1, 4);
        v.add_edge(2, 5);
        v.add_edge(1, 6);
        v.add_edge(2, 6);
        v.add_edge(4, 6);
        v.add_edge(6, 7);
        v.add_edge(5, 7);
        let mut h = Base::default();
        assert!(h.reduce_dominance(&v));
        eprintln!("{:?}", h.chosen);
        assert_eq!(
            h.chosen,
            vec![0, 5, 6].iter().map(|i| NodeIndex::new(*i)).collect()
        );
        let b = !h.reduce_dominance(&v);
        eprintln!("{:?}", h.chosen);
        assert!(b);
    }

    #[test]
    fn remove_dominant_vertices_small() {
        let mut v = VertexCover::from_size(4);
        v.add_edge(0, 1);
        v.add_edge(0, 2);
        v.add_edge(0, 3);
        v.add_edge(1, 2);
        v.add_edge(2, 3);
        let mut h = Base::default();
        assert!(h.reduce_dominance(&v));
        eprintln!("{:?}", h.chosen);
        assert_eq!(
            h.chosen,
            vec![0, 2].iter().map(|i| NodeIndex::new(*i)).collect()
        );
        let b = !h.reduce_dominance(&v);
        eprintln!("{:?}", h.chosen);
        assert!(b);
    }
}
