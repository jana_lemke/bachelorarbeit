use petgraph::prelude::*;
use petgraph::stable_graph::{Neighbors, NodeIndices};
use rand::prelude::SliceRandom;
use std::collections::HashSet;
use std::fmt::Write;

#[derive(Clone, Debug)]
pub struct VertexCover {
    size: usize,
    vertices: Vec<NodeIndex>,
    graph: StableUnGraph<(), ()>,
    cached_edges: Option<Vec<(NodeIndex, NodeIndex)>>,
    cached_sorted_edges: Option<Vec<(NodeIndex, NodeIndex)>>,
}

impl VertexCover {
    pub fn from_edges(edges: &[(usize, usize)]) -> Self {
        let size = edges.iter().fold(0, |a, (i, j)| a.max(*i.max(j))) + 1_usize;
        let mut v = Self::from_size(size);
        v.add_edges(edges);
        v
    }

    pub fn from_adj_list(adj_list: &str) -> Self {
        let edges: Vec<(usize, usize)> = adj_list
            .split('\n')
            .enumerate()
            .map(|(number, line)| {
                line.split_ascii_whitespace()
                    .map(move |i| (number, i.parse::<usize>().unwrap() - 1))
            })
            .flatten()
            .collect();
        Self::from_edges(&edges)
    }

    pub fn from_size(size: usize) -> Self {
        let mut graph = StableGraph::default();
        let vertices = (0..size).map(|_| graph.add_node(())).collect();
        VertexCover {
            size,
            vertices,
            graph,
            cached_edges: None,
            cached_sorted_edges: None,
        }
    }

    /// edges as `p <i> <j>` if there is an edge between `i` and `j`
    pub fn from_dimacs(dimacs: &str) -> Self {
        let edges: Vec<_> = dimacs
            .lines()
            .filter_map(|line| {
                if line.is_empty() {
                    return None;
                };
                let parts: Vec<_> = line.split_ascii_whitespace().collect();
                if (parts[0] == "e" || parts[0] == "p") && parts.len() == 3 {
                    Some((
                        parts[1].parse::<usize>().unwrap() - 1,
                        parts[2].parse::<usize>().unwrap() - 1,
                    ))
                } else {
                    None
                }
            })
            .collect();
        Self::from_edges(&edges)
    }

    /// Read the format of file downloaded directly from here: https://snap.stanford.edu/data/index.html
    ///
    /// If using files from https://sparse.tamu.edu/SNAP (which is a mirror with some instances added)
    /// download the Matrix Market format and use the [from_matrix_market method](struct.VertexCover.html#method.from_matrix_market)
    /// edges as `<i> <j>`; comments start with `#`
    pub fn from_stanford(stanford: &str) -> Self {
        let edges: Vec<_> = stanford
            .lines()
            .filter(|line| !(line.is_empty() || line.starts_with('%')))
            .map(|line| {
                let parts: Vec<_> = line.split_ascii_whitespace().collect();
                (
                    parts[0].parse::<usize>().unwrap() - 1,
                    parts[1].parse::<usize>().unwrap() - 1,
                )
            })
            .collect();
        Self::from_edges(&edges)
    }

    /// Read the format of file downloaded directly from here: https://sparse.tamu.edu/SNAP (
    /// edges as `<i> <j>`; comments start with `%`; the first line after the inital comments is metadata about the number of nodes
    pub fn from_matrix_market(mm: &str) -> Self {
        let edges: Vec<_> = mm
            .lines()
            .filter(|line| !(line.is_empty() || line.starts_with('%')))
            .skip(1)
            .map(|line| {
                let parts: Vec<_> = line.split_ascii_whitespace().collect();
                (
                    parts[0].parse::<usize>().unwrap() - 1,
                    parts[1].parse::<usize>().unwrap() - 1,
                )
            })
            .collect();
        Self::from_edges(&edges)
    }

    pub fn from_gr(mm: &str) -> Self {
        let edges: Vec<_> = mm
            .lines()
            .filter(|line| !(line.is_empty() || line.starts_with('%')))
            .skip(1)
            .map(|line| {
                let parts: Vec<_> = line.split_ascii_whitespace().collect();
                (
                    parts[0].parse::<usize>().unwrap() - 1,
                    parts[1].parse::<usize>().unwrap() - 1,
                )
            })
            .collect();
        Self::from_edges(&edges)
    }

    pub fn to_dimacs(&self) -> String {
        let mut s = String::new();
        writeln!(s, "n e {} {}", self.node_count(), self.edge_count()).unwrap();
        for i in 0..self.size {
            for j in i + 1..self.size {
                if self.graph.contains_edge(self.vertices[i], self.vertices[j]) {
                    writeln!(s, "p {} {}", i + 1, j + 1).unwrap();
                }
            }
        }
        s
    }

    pub fn to_gr(&self) -> String {
        let mut s = String::new();
        writeln!(s, "p td {} {}", self.node_count(), self.edge_count()).unwrap();
        for i in 0..self.size {
            for j in i + 1..self.size {
                if self.graph.contains_edge(self.vertices[i], self.vertices[j]) {
                    writeln!(s, "{} {}", i + 1, j + 1).unwrap();
                }
            }
        }
        s
    }

    pub fn unsat_npbench_50_1() -> Self {
        Self::from_dimacs(include_str!("graph50-01.txt"))
    }

    /// Taken from here: https://github.com/sangyh/minimum-vertex-cover/blob/master/Data/karate.graph
    pub fn karate_graph() -> Self {
        VertexCover::from_adj_list(
            "2 3 4 5 6 7 8 9 11 12 13 14 18 20 22 32
    1 3 4 8 14 18 20 22 31
    1 2 4 8 9 10 14 28 29 33
    1 2 3 8 13 14
    1 7 11
    1 7 11 17
    1 5 6 17
    1 2 3 4
    1 3 31 33 34
    3 34
    1 5 6
    1
    1 4
    1 2 3 4 34
    33 34
    33 34
    6 7
    1 2
    33 34
    1 2 34
    33 34
    1 2
    33 34
    26 28 30 33 34
    26 28 32
    24 25 32
    30 34
    3 24 25 34
    3 32 34
    24 27 33 34
    2 9 33 34
    1 25 26 29 33 34
    3 9 15 16 19 21 23 24 30 31 32 34
    9 10 14 15 16 19 20 21 23 24 27 28 29 30 31 32 33 ",
        )
    }

    pub fn random(size: usize, density: usize) -> Self {
        let mut edges = Vec::new();
        for i in 0..size {
            for j in i + 1..size {
                if rand::random::<u32>() < u32::MAX / (100 / density) as u32 {
                    edges.push((i, j))
                }
            }
        }
        let mut v = Self::from_size(size);
        v.add_edges(&edges);
        v
    }

    pub fn barabasi_albert(size: usize, m: usize) -> Self {
        let mut rng = rand::thread_rng();
        let mut instance = VertexCover::from_size(m);
        let mut targets: Vec<_> = (0..m).collect();
        let mut repeated_nodes: Vec<usize> = Vec::new();
        let mut source = m;
        while source < size {
            let source_repeated = std::iter::repeat(source).take(m);
            instance.add_node();
            //eprintln!("{:?}", instance.vertices);
            //eprintln!("{:?}", repeated_nodes);
            instance.add_edges(
                &(&targets)
                    .iter()
                    .copied()
                    .zip(source_repeated.clone())
                    .collect::<Vec<_>>(),
            );
            repeated_nodes.extend(&targets);
            repeated_nodes.extend(source_repeated);
            targets = repeated_nodes
                .choose_multiple(&mut rng, m)
                .copied()
                .collect();
            source += 1;
        }

        instance
    }
    fn add_edges(&mut self, edges: &[(usize, usize)]) {
        for (i, j) in edges {
            self.add_edge(*i, *j);
        }
    }

    pub fn add_edge(&mut self, i: usize, j: usize) {
        let a = i.min(j);
        let b = i.max(j);
        if i != j {
            //&& !self.graph.contains_edge(self.vertices[a], self.vertices[b]) {
            self.graph.add_edge(self.vertices[a], self.vertices[b], ());
        }
    }

    pub fn remove_edge(&mut self, i: usize, j: usize) {
        self.graph.remove_edge(
            self.graph
                .find_edge(self.vertices[i], self.vertices[j])
                .unwrap(),
        );
    }

    pub fn edge_count(&self) -> usize {
        self.graph.edge_count()
    }

    /// Returns an edge in the graph
    ///
    /// It is not specified which edge.
    pub fn get_edge(&self) -> Option<(NodeIndex, NodeIndex)> {
        self.edges().into_iter().next()
    }

    pub fn edges(&self) -> Vec<(NodeIndex, NodeIndex)> {
        if let Some(edges) = &self.cached_edges {
            edges.clone()
        } else {
            self.graph
                .edge_indices()
                .map(|e| self.graph.edge_endpoints(e))
                .flatten()
                .collect()
        }
    }

    pub fn contains_edge(&self, a: NodeIndex, b: NodeIndex) -> bool {
        self.graph.contains_edge(a, b)
    }

    pub fn cache_sorted_edges(&mut self) {
        let mut edges = self.edges();
        edges.sort_by_cached_key(|&(a, b)| self.degree(a) + self.degree(b));
        self.cached_sorted_edges = Some(edges);
    }

    pub fn cache_edges(&mut self) {
        self.cached_edges = Some(self.edges());
    }

    pub fn with_cache(mut self) -> Self {
        self.cache_edges();
        self.cache_sorted_edges();
        self
    }

    pub fn sorted_edges(&self) -> Vec<(NodeIndex, NodeIndex)> {
        if let Some(edges) = &self.cached_sorted_edges {
            edges.clone()
        } else {
            let mut edges = self.edges();
            edges.sort_by_cached_key(|&(a, b)| self.degree(a) + self.degree(b));
            edges
        }
    }

    pub fn nodes(&self) -> NodeIndices<()> {
        self.graph.node_indices()
    }

    pub fn get_node(&self) -> Option<usize> {
        self.graph
            .node_indices()
            .map(|n| self.vertices.iter().position(|&x| x == n).unwrap())
            .next()
    }

    pub fn node_count(&self) -> usize {
        self.graph.node_count()
    }

    pub fn add_node(&mut self) {
        let idx = self.graph.add_node(());
        self.vertices.push(idx);
        self.size += 1;
    }

    pub fn remove_node(&mut self, vertex: usize) {
        self.graph.remove_node(self.vertices[vertex]);
    }

    pub fn get_neighbours(&self, vertex: NodeIndex) -> Neighbors<()> {
        self.graph.neighbors(vertex)
    }

    pub fn degree(&self, vertex: NodeIndex) -> usize {
        self.graph.neighbors(vertex).count()
    }

    pub fn size(&self) -> usize {
        self.size
    }

    fn translate_vertex(&self, index: &NodeIndex) -> usize {
        self.vertices.iter().position(|x| x == index).unwrap()
    }
}

pub trait Problem {
    type Choice;
    type Solution;
    type Intermediate;

    fn size(&self) -> usize;
    fn translate_solution(&self, solution: Self::Intermediate) -> Self::Solution;
}

impl Problem for VertexCover {
    type Choice = NodeIndex<u32>;
    type Solution = HashSet<usize>;
    type Intermediate = HashSet<NodeIndex>;
    fn size(&self) -> usize {
        self.size
    }

    fn translate_solution(&self, solution: HashSet<NodeIndex>) -> <Self as Problem>::Solution {
        solution.iter().map(|x| self.translate_vertex(x)).collect()
    }
}

pub trait Solution {
    fn size(&self) -> usize;
}

impl Solution for HashSet<usize> {
    fn size(&self) -> usize {
        self.len()
    }
}

impl Solution for HashSet<NodeIndex> {
    fn size(&self) -> usize {
        self.len()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn dimacs_output_equals_input() {
        let unsat50 = include_str!("graph50-01.txt");
        assert_eq!(unsat50, VertexCover::from_dimacs(unsat50).to_dimacs())
    }
}
