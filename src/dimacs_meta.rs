#[macro_export]
macro_rules! custom_criterion_group {
    (name = $name:ident; stats; config = $config:expr; targets = $( $target:path ),+ $(,)*) => {
        pub fn $name() {
            let stats = Statistics::default();
            let mut criterion: Criterion<StatisticsWrapper> = $config
                .with_measurement(StatisticsWrapper::new(&stats))
                .configure_from_args()
                .without_plots();
            $(
                $target(&mut criterion, &stats);
            )+
        }
    };
    (name = $name:ident; config = $config:expr; targets = $( $target:path ),+ $(,)*) => {
        pub fn $name() {
            let mut criterion: $crate::Criterion<_> = $config
                .configure_from_args();
            $(
                $target(&mut criterion);
            )+
        }
    };
    ($name:ident, $( $target:path ),+ $(,)*) => {
        criterion_group!{
            name = $name;
            config = $crate::Criterion::default();
            targets = $( $target ),+
        }
    }
}

#[macro_export]
macro_rules! bench_dimacs_file {
    ($name:ident, $filename:expr, $heuristic:expr, time) => {
        pub fn $name(c: &mut Criterion) {
            let instance = VertexCover::from_dimacs(include_str!($filename)).with_cache();
            c.bench_function(
                &format!("time/{}/{}", stringify!($name), stringify!($heuristic)),
                |b| {
                    b.iter_batched(
                        || instance.clone(),
                        |instance| Solver::default().branch_and_bound_vertex(instance, $heuristic),
                        criterion::BatchSize::SmallInput,
                    )
                },
            );
        }
    };

    ($name:ident, $filename:expr, $heuristic:expr, nodes) => {
        pub fn $name(c: &mut Criterion<StatisticsWrapper>, stats: &Statistics) {
            let instance = VertexCover::from_dimacs(include_str!($filename)).with_cache();
            let mut group = c.benchmark_group(&format!(
                "nodes/{}/{}",
                stringify!($name),
                stringify!($heuristic)
            ));
            group.bench_function(".", |b| {
                b.iter_batched(
                    || instance.clone(),
                    |instance| {
                        Solver::with_statistics(stats).branch_and_bound_vertex(instance, $heuristic)
                    },
                    criterion::BatchSize::SmallInput,
                )
            });
        }
    };
}
pub const SAMPLE_SIZE: usize = 10;
