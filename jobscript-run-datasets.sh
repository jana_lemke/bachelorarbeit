#!/usr/local_rwth/bin/zsh
set -eu
IFS=$'\n\t'

function benchmark_commit() {
    local file=$1
    local ext=$file:t:e
    if [[ $ext == "mtx" ]]
    then
        format=mm
    else
        format=dimacs
    fi
    hash="$(git log -1 --format="%h")"
    local id="$(basename $file)-$(date --iso-8601=ns)-$hash"
    local script="#!/usr/local_rwth/bin/zsh
#SBATCH --mem-per-cpu=16G
#SBATCH --job-name=$file
#SBATCH --output=logs/output.$id.txt
#SBATCH --time 40:00:00
cat $file | target/release/main -f $format -h all -d -N
"
        echo "
[Submitted $id]
$script" >> logs/benchmark-log
        echo "$script" | sbatch;
}


if [[ ! -d logs ]]; then
    mkdir logs
fi

cargo build --release

for file do
    benchmark_commit $file
done
