# Benchmarks
## Old Method using Criterion
Benchmarks are using `criterion-rs`. They can be run with `cargo bench`. This runs binaries generated from `.rs` files in `benches`. You can pass options to these binaries using `cargo bench -- <options>`.
One of the options you can pass is a regex that benchmark names must match to be actually executed (the binary will still be called).
`karate-{nodes,time}.rs` are benchmarks using a smallish instance I found online here https://github.com/sangyh/minimum-vertex-cover/blob/master/Data/karate.graph.
`random-{nodes,time}.rs` are benchmarks on graphs that are randomly generated for each iteration.
`dimacs.rs` is generated by the somewhat ugly script `benches/codegen.py`, uses macros defined in `src/dimacs_meta.rs` and executes benchmarks on instances in DIMACS format saved in `benches/dimacs/`.

To run benchmarks on the RWTH Cluster you can use the `jobscript-benchmark-commits.sh`. It will submit sequentially running jobs for all benchmarks that exist in all git refs (or more precisely anything that can be passed to `git checkout`) you pass to `jobscript-benchmark-commits.sh` (for example `jobscript-benchmark-commits.sh master` will run all benchmarks existing in the last commit in the master branch). For other systems using SLURM you probably only need to change the shebang.

### Measurements

Benchmarks with `time` in the name are using the standard criterion measurement.
Benchmarks with `nodes` in the name are using a custom measurement defined in `src/solver.rs` that counts the nodes of the search tree the solver evaluates.

## New Method with `Instant::now()`
Since we are now benching only existing instances we don't need the fancier features of `criterion-rs` (the statistical analysis including hypothesis testing) and the interface (one function per benchmark) is a bit clunky, so we instead just rely on manually timing with `Instant::now()` and running our own basic analysis on the data afterwards. Timing is implemented in `main.rs`.

Datasets include https://snap.stanford.edu/data/index.html. This has a mirror with more instances here https://sparse.tamu.edu/SNAP to easily download from the latter use `ssgui` or one of the other interfaces available at https://sparse.tamu.edu/interfaces.

`jobscript-run-datasets.sh` submits batch jobs for timing all provided filenames with all heuristics.