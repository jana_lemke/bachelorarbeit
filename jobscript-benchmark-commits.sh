#!/usr/local_rwth/bin/zsh
set -eu
IFS=$'\n\t'

function benchmark_commit() {
    local commit=$1
    local id="$commit-$(date --iso-8601=m)"
    local script="#!/usr/local_rwth/bin/zsh
#SBATCH --mem-per-cpu=4G
#SBATCH --job-name=BENCHMARK
#SBATCH --dependency=singleton
#SBATCH --output=logs/output.$id.txt
#SBATCH --time 50:00:00

git checkout $commit
hash=\"\$(git log -1 --format="%h")\"
cargo bench -- --save-baseline \$hash
"
    echo "
[Submitted $id]" "$script" >> logs/benchmark-log
    echo "$script"
}


if [[ ! -d logs ]]; then
    mkdir logs
fi

for commit do
    echo "$(benchmark_commit $commit)" | sbatch
done
